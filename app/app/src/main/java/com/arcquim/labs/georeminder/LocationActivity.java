package com.arcquim.labs.georeminder;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;

/**
 * Created by Aleksandr Erin on 24.12.2017.
 */

public class LocationActivity extends AppCompatActivity {

    String[] names = { "Иван7uyujrhnjsgcf jhvnfhrhtkjcfhmsajdakjsdmxjkjdksdjknvhschmfjksxkjhkjvhdfkjbnvmnhjcjkxhkjcshjkvhkjjfghkjsdhkjshgvkjdfvjkdnvkjdvnkdjfvnkjdvnjdkfvnjdfvndjfmvdnfjmvbdjfmjvb", "Марья", "Петр", "Антон", "Даша", "Борис",
            "Костя", "Игорь", "Анна", "Денис", "Андрей" };

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);
        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);

        Intent intent = getIntent();
        toolbar.setTitle(intent.getStringExtra("title"));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(intent.getStringExtra("title"));

        final ListView remindersListView = (ListView) findViewById(R.id.reminders_list);
        ListAdapter adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, names);
        remindersListView.setAdapter(adapter);
        remindersListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Object item = parent.getItemAtPosition(position);
                LocationActivity.this.showReminder(item);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                break;
            case R.id.add_reminder:
                createNewReminder();
                return true;
            case R.id.delete_location:
                deleteLocation();
        }
        return super.onOptionsItemSelected(item);
    }

    private void createNewReminder() {
        Intent intent = new Intent(this, ReminderActivity.class);
        intent.putExtra("message", "");
        intent.putExtra("location", 1);
        startActivity(intent);
    }

    private void deleteLocation() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Are you sure you want to delete this location?");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                //TODO: remove location
                LocationActivity.super.onBackPressed();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_location, menu);
        return true;
    }

    private void showReminder(Object reminder) {
        if (reminder != null) {
            Intent intent = new Intent(this, ReminderActivity.class);
            intent.putExtra("message", reminder.toString());
            startActivity(intent);
        }
    }
}
