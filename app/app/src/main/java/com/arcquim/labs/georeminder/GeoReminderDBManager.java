package com.arcquim.labs.georeminder;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 173042 on 26.12.2017.
 */

public class GeoReminderDBManager {

    private SQLiteDatabase database;

    public static final int EARTH_RADIUS = 6371;

    public GeoReminderDBManager(SQLiteDatabase database) {
        this.database = database;
    }

    public static class LocationModel {
        private int id;
        private String title;
        private double latitude;
        private double longitude;

        public LocationModel(int id, String title, double lat, double longitude) {
            this.id = id;
            this.title = title;
            this.latitude = lat;
            this.longitude = longitude;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public double getLatitude() {
            return latitude;
        }

        public void setLatitude(double latitude) {
            this.latitude = latitude;
        }

        public double getLongitude() {
            return longitude;
        }

        public void setLongitude(double longitude) {
            this.longitude = longitude;
        }

        @Override
        public String toString() {
            return title;
        }
    }

    public static class ReminderModel {
        private int id;
        private String text;
        private int locationId;

        public ReminderModel(int id, String text, int locationId) {
            this.id = id;
            this.text = text;
            this.locationId = locationId;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public int getLocationId() {
            return locationId;
        }

        public void setLocationId(int locationId) {
            this.locationId = locationId;
        }

        @Override
        public String toString() {
            return text;
        }
    }

    public List<LocationModel> getLocations(String nameToContain) {
        if (nameToContain == null) {
            nameToContain = "";
        }
        nameToContain = '%' + nameToContain.toLowerCase() + '%';
        Cursor cursor = database.query(
                "locations",
                new String[]{"_id", "name", "latitude", "longitude"},
                "lower(name) like ?", new String[]{nameToContain}, null, null,
                "name ASC");
        List<LocationModel> locationModels = new ArrayList<>();
        boolean thereAreRecords = cursor.moveToFirst();
        while (thereAreRecords) {
            locationModels.add(new LocationModel(
                    cursor.getInt(cursor.getColumnIndexOrThrow("_id")),
                    cursor.getString(cursor.getColumnIndexOrThrow("name")),
                    cursor.getDouble(cursor.getColumnIndexOrThrow("latitude")),
                    cursor.getDouble(cursor.getColumnIndexOrThrow("longitude"))
            ));
            thereAreRecords = cursor.moveToNext();
        }
        return locationModels;
    }

    public List<ReminderModel> getAllRemindersOfLocation(int locationId) {
        Cursor cursor = database.query(
                "reminders",
                new String[]{"_id", "text", "location_id"},
                "location_id=?", new String[]{Integer.toString(locationId)},
                null, null,
                "_id DESC");
        List<ReminderModel> locationModels = new ArrayList<>();
        boolean thereAreRecords = cursor.moveToFirst();
        while (thereAreRecords) {
            locationModels.add(new ReminderModel(
                    cursor.getInt(cursor.getColumnIndexOrThrow("_id")),
                    cursor.getString(cursor.getColumnIndexOrThrow("text")),
                    cursor.getInt(cursor.getColumnIndexOrThrow("location_id"))
            ));
            thereAreRecords = cursor.moveToNext();
        }
        return locationModels;
    }

    public List<ReminderModel> getAllRemindersNearLocation(LatLng latLng, double radius) {
        double maxLatitude = latLng.latitude + rad2deg(radius / EARTH_RADIUS);
        double minLatitude = latLng.latitude - rad2deg(radius / EARTH_RADIUS);
        double maxLongitude = latLng.longitude + rad2deg(Math.asin(radius / EARTH_RADIUS) / Math.cos(deg2rad(latLng.latitude)));
        double minLongitude = latLng.longitude - rad2deg(Math.asin(radius / EARTH_RADIUS) / Math.cos(deg2rad(latLng.latitude)));
        Cursor cursor = database.query(
                "reminders",
                new String[]{"_id", "text", "location_id"},
                "location_id in (SELECT _id FROM locations WHERE latitude BETWEEN ? AND ? AND longitude BETWEEN ? AND ?)",
                new String[]{
                        Double.toString(minLatitude),
                        Double.toString(maxLatitude),
                        Double.toString(minLongitude),
                        Double.toString(maxLongitude)
                },
                null, null,
                "_id DESC");
        List<ReminderModel> locationModels = new ArrayList<>();
        boolean thereAreRecords = cursor.moveToFirst();
        while (thereAreRecords) {
            locationModels.add(new ReminderModel(
                    cursor.getInt(cursor.getColumnIndexOrThrow("_id")),
                    cursor.getString(cursor.getColumnIndexOrThrow("text")),
                    cursor.getInt(cursor.getColumnIndexOrThrow("location_id"))
            ));
            thereAreRecords = cursor.moveToNext();
        }
        return locationModels;
    }

    public void createLocation(LatLng latLng, String name) {
        ContentValues values = new ContentValues();
        values.put("name", name);
        values.put("latitude", latLng.latitude);
        values.put("longitude", latLng.longitude);
        database.insert("locations", "_id", values);
    }

    public static double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    public static double rad2deg(double rad) {
        return rad * 57.29577951308232;
    }

    public void createReminder(int locationId, String text) {
        ContentValues values = new ContentValues();
        values.put("text", text);
        values.put("location_id", locationId);
        database.insert("reminders", "_id", values);
    }

    public void deleteLocation(int locationId) {
        database.delete("locations", "_id=?", new String[]{Integer.toString(locationId)});
    }

    public void deleteReminder(int reminderId) {
        database.delete("reminders", "_id=?", new String[]{Integer.toString(reminderId)});
    }

    public void updateLocation(int locationId, String newName) {
        ContentValues values = new ContentValues();
        values.put("name", newName);
        database.update("locations", values, "_id=?", new String[]{Integer.toString(locationId)});
    }

    public void updateReminder(int reminderId, String newText) {
        ContentValues values = new ContentValues();
        values.put("text", newText);
        database.update("reminders", values, "_id=?", new String[]{Integer.toString(reminderId)});
    }

}
