package com.arcquim.labs.georeminder;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Aleksandr Erin on 25.12.2017.
 */

public class GeoReminderDBHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "GeoReminder.db";
    public static final String SQL_CREATE_LOCATIONS_TABLE = "CREATE TABLE locations (" +
            "_id INTEGER PRIMARY KEY AUTOINCREMENT," +
            "name TEXT NOT NULL," +
            "latitude REAL NOT NULL," +
            "longitude REAL NOT NULL)";
    public static final String SQL_CREATE_REMINDERS_TABLE = "CREATE TABLE reminders (" +
            "_id INTEGER PRIMARY KEY AUTOINCREMENT," +
            "text TEXT NOT NULL," +
            "location_id INTEGER NOT NULL," +
            "FOREIGN KEY(_id) REFERENCES locations(_id))";

    public GeoReminderDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_LOCATIONS_TABLE);
        db.execSQL(SQL_CREATE_REMINDERS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
